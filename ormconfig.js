const devEnv = {
  type: "postgres",
  host: process.env.PG_HOST,
  port: "5432",
  database: process.env.PG_DB,
  username: process.env.PG_USERNAME,
  password: process.env.PG_PASSWORD,
  entities: ["./src/entities/**/*.ts"],
  migrations: ["./src/database/migrations/*.ts"],
  cli: {
    migrationsDir: "./src/database/migrations",
  },
  logging: true,
  synchronize: false,
};

const testEnv = {
  type: "sqlite",
  database: ":memory:",
  entities: ["./src/entities/**/*.ts"],
  synchronize: true,
};

let exportModule;

if (process.env.NODE_ENV === "test") {
  exportModule = testEnv;
} else {
  exportModule = devEnv;
}

module.exports = exportModule;
